# Deployment

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 're-deployment', :git => 'https://bitbucket.org/ikitlab/re-deployment.git'

And then execute:

    $ bundle

Generate base template

    $ capify .
    $ rails g deployment:install


Or install it yourself as:

    $ gem install re-deployment

## Usage


### On server side
Some steps for creating users(deployer, jenkins, etc...)
Cloning .ssh folder if required

login by created user
install RVM

    $ gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3
    $ \curl -sSL https://get.rvm.io | bash
    $ rvm install "ruby-version"

TODO: Move rvm, ruby installer in recipes

### On local machine
in project root directory. NOTICE!!! all commands must be with environment!!! (example: cap development deploy)


    $ cap deploy:install  # install all base libraries (postgres, nginx, imagemagick, nodejs)
    $ cap deploy:setup   # setup config files(puma, nginx), creates databases(postgresql)

Useful commands

    $ cap -T   # check all cap tasks

    $ cap rails:invoke task=some_task   # execute task on server

TODO: write logs recipes
## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
