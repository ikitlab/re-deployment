# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'deployment/version'

Gem::Specification.new do |spec|
  spec.name          = "re-deployment"
  spec.version       = Deployment::VERSION
  spec.authors       = ["Ilya Kraislnikov"]
  spec.email         = ["ilya@ikitlab.com"]
  spec.description   = %q{ikitlab default deployment script}
  spec.summary       = %q{ikitlab default deployment script}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_dependency "capistrano", "< 3.0"
  # spec.add_dependency "capistrano-ext"
  spec.add_dependency "capistrano_colors"
  spec.add_dependency "rvm-capistrano"
  spec.add_dependency "puma"

end
