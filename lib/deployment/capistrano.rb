require 'capistrano_colors'
require 'bundler/capistrano'
require "rvm/capistrano"

Capistrano::Configuration.instance.load do
  # we use assets compilation
  load 'deploy/assets'

  ## COMMON DEFINITIONS

  # ssh settings
  # possible options from here: http://net-ssh.github.io/net-ssh/classes/Net/SSH.html
  ssh_options[:auth_methods]          = ["publickey"]
  ssh_options[:keys]                  = [ENV['SSH_KEY_FILE']] unless ENV['SSH_KEY_FILE'].nil?
  ssh_options[:forward_agent]         = true
  ssh_options[:paranoid]              = false
  ssh_options[:port]                  = 22
  default_run_options[:pty]           = true

  # identification
  set(:user)                          { application }

  # repository
  set(:scm)                           { :git }
  set(:scm_verbose)                   { false }
  set(:deploy_via)                    { :remote_cache }
  set(:copy_exclude)                  { [ '.git' ] }

  set(:repository)                    { "git@bitbucket.org:ikitlab/#{application}.git" }
  set(:branch)                        { "master" }

  # directories
  set(:deploy_to)                     { "/srv/#{application}" }
  set(:shared_children)               { (fetch(:more_shared_children, []) + %w(log tmp/pids public/system public/assets public/uploads)).uniq }

  # other settings
  set(:rails_env)                     { "production" }
  set(:use_sudo)                      { false }
  set(:keep_releases)                 { 10 }
  set(:rake)                          { "RAILS_ENV=#{rails_env} #{fetch(:bundle_cmd, 'bundle')} exec rake --trace" }
  set(:normalize_asset_timestamps)    {false }


  # shared settings
  set(:shared_settings)               { (fetch(:more_shared_settings, []) + %w(database.yml puma.rb)).uniq }

  # RVM: TODO
  # set :rvm_type, :user
  # set :rvm_ruby_string,               :local
  # set :rvm_autolibs_flag, 1


  ## STAGES

  # all possible stages
  set(:default_stages)                { %w(ikitlab-dev ikitlab-test ikitlab-stage) }
  set(:stages)                        { (fetch(:additional_stages, []) + fetch(:default_stages, [])).map(&:downcase).uniq }

  # Should define stage tasks as soon as posible, before any option in this
  # file is set
  #TODO: refactor stages and default stages
  stages.each do |name|
    desc "Set the target stage to `#{name}'."
    task(name) do
      set :stage,   name.to_sym
      set :domain,  'example.com'
      if fetch(:additional_stages, []).include?(name)
        # load non-default stages from file
        load "config/deploy/#{name}" if file_in_load_path?("config/deploy/#{name}")
      else
        # the other stages settings here
        ## TODO: think about this 01
        set  :domain, "#{application}.#{name.gsub(/ikitlab-/, '')}.lan.ikitlab.co"
        role :web,    "#{user}@#{domain}"
        role :app,    "#{user}@#{domain}"
        role :db,     "#{user}@#{domain}", :primary => true
      end
    end
  end

  on :load do
    # The first non option argument
    env = ARGV.detect { |a| a.to_s !~ /\A-/ && a.to_s !~ /=/ }
    if stages.include?(env)
      # Execute the specified stage so that recipes required in stage can contribute to task list
      find_and_execute_task(env) if ARGV.any?{ |option| option =~ /-T|--tasks|-e|--explain/ }
    else
      abort 'Wrong stage specified.'
    end
  end

  ## HOOKS

  # copy secured configuration files from shared/ dir to release
  after "deploy:finalize_update", roles: :app do
    shared_settings.each do |f|
      run <<-CMD
        if [ -f #{shared_path}/config/#{f} ]; then \
          rm -f #{release_path}/config/#{f}; \
          mkdir -p #{File.dirname(File.join(release_path, 'config', f))}; \
          cp -a #{shared_path}/config/#{f} #{release_path}/config/#{f}; \
        else \
          echo '#{f} does not exist!'; \
          false; \
        fi
      CMD
    end
  end


  # set build version
  after "deploy:create_symlink",  roles: :app do
    run "echo BUILD_VERSION = \\'#{fetch(:revision, 'n/a')}\\' > #{release_path}/config/initializers/version.rb"
  end

  namespace :deploy do
    desc "Load the seed data"
    task :seed do
      run "cd #{release_path}; #{rake} db:seed"
    end
  end

  # triggering callbacks
  after "deploy:update_code", "deploy:migrate"
  after "deploy:update_code", "deploy:seed"
  after "deploy:restart",     "deploy:cleanup"

  namespace :web do
    task :disable, :roles => :app do
      on_rollback { run "rm #{release_path}/public/maintenance.html; exit 0" }
      run "ln -s #{release_path}/public/_maintenance.html #{release_path}/public/maintenance.html; true"
    end
    task :enable, :roles => :app do
      run "rm #{release_path}/public/maintenance.html; true"
    end
  end

end
