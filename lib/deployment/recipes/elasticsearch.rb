Capistrano::Configuration.instance.load do

  set_default(:elasticsearch_version, '1.5.0')

  namespace :elasticsearch do

    desc "Install elasticsearch"
    task :install, roles: :web do
      run "#{sudo} apt-get install openjdk-7-jre-headless -y"
      run "wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-#{elasticsearch_version}.deb"
      run "#{sudo} dpkg -i elasticsearch-#{elasticsearch_version}.deb"
      run 'rm elasticsearch-*'
    end
    after "deploy:full_install", "elasticsearch:install"

    desc "Install elasticsearch"
    task :setup, roles: :web do
      run "echo 'network.host: localhost' | #{sudo} tee --append /etc/elasticsearch/elasticsearch.yml"
      start
    end
    after "deploy:full_setup", "elasticsearch:setup"


    %w[start stop restart status].each do |command|
      desc "#{command} elasticsearch"
      task command, roles: :web do
        run "#{sudo} service elasticsearch #{command}"
      end
    end

  end
end
