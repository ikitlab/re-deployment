def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

def add_apt_repository(repo)
  run "#{sudo} add-apt-repository #{repo}", :pty => true do |ch, stream, data|
    if data =~ /Press.\[ENTER\].to.continue/
      ch.send_data("\n")
    else
      Capistrano::Configuration.default_io_proc.call(ch, stream, data)
    end
  end
end

Capistrano::Configuration.instance.load do

  namespace :deploy do
    desc "Install everything onto the server"
    task :install do
      run "#{sudo} apt-get -y update"
      run "#{sudo} apt-get -y upgrade"
      run "#{sudo} apt-get -y install python-software-properties software-properties-common ufw"
      run "#{sudo} apt-get -y install default-jdk"
      run "#{sudo} apt-get -y install git"
    end

    task :setup_app_conf do
      run "#{sudo} mkdir -p /srv/#{application}"
      run "#{sudo} chown #{user}:sudo /srv/#{application}"
      run "mkdir -p #{shared_path}/log"
      run "mkdir -p #{shared_path}/sockets"
    end

    desc 'If you have slow server, run this task for adding swap'
    task :setup_swap do
      run "#{sudo} fallocate -l 4G /swapfile"
      run "#{sudo} chmod 600 /swapfile"
      run "#{sudo} mkswap /swapfile"
      run "#{sudo} swapon /swapfile"
    end



    before 'deploy:setup', 'deploy:setup_app_conf'
  end
end

