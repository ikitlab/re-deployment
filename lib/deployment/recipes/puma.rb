Capistrano::Configuration.instance.load do

  set_default(:puma_cmd)            { "#{fetch(:bundle_cmd, 'bundle')} exec puma" }
  set_default(:pumactl_cmd)         { "#{fetch(:bundle_cmd, 'bundle')} exec pumactl" }
  set_default(:puma_state)          { "#{shared_path}/sockets/puma.state" }
  set_default(:puma_workers)        { 2 }

  set :puma_config_file,    './config/puma.rb'

  namespace :puma do

    desc "Setup puma configuration for this application"
    task :setup, roles: :web do
      #TODO: move in templates
      contents = <<-FILE
      daemonize
      environment           '#{rails_env}'
      directory             '#{current_path}'
      state_path            '#{puma_state}'
      bind                  'unix://#{shared_path}/sockets/puma.sock'
      activate_control_app  'unix://#{shared_path}/sockets/pumactl.sock'

      ## BUG: https://github.com/puma/puma/issues/359
      # stdout_redirect       '#{shared_path}/log/puma.stdout', '#{shared_path}/log/puma.stderr', true

      threads 8, 32
      workers #{puma_workers}
      preload_app!
      on_worker_boot do
        ActiveSupport.on_load(:active_record) do
          ActiveRecord::Base.establish_connection
        end
      end
      FILE

      put(contents, File.expand_path(File.join(shared_path, puma_config_file)))

    end

    desc 'Start puma'
    task :start, roles: :app, on_no_matching_servers: :continue do
      run "cd #{current_path} && #{puma_cmd} -C #{puma_config_file}", :pty => false
    end

    desc 'Stop puma'
    task :stop, roles: :app, on_no_matching_servers: :continue do
      run "cd #{current_path} && #{pumactl_cmd} -S #{puma_state} stop"
    end

    desc 'Restart puma'
    task :restart, roles: :app, on_no_matching_servers: :continue do
      begin
        stop
        run 'sleep 5'
          # run "cd #{current_path} && #{pumactl_cmd} -S #{puma_state} phased-restart"
      rescue => ex
        puts "Failed to stop puma: #{ex}\nAssuming not started."
      end
      start
    end

    after 'deploy:setup',   'puma:setup'
    
    after 'deploy:stop',    'puma:stop'
    after 'deploy:start',   'puma:start'
    after 'deploy:restart', 'puma:restart'

  end

end
