Capistrano::Configuration.instance.load do

  namespace :rails do
    desc "Remote console"
    task :console, :roles => :app do
      run_interactively "bundle exec rails console #{rails_env}"
    end

    desc "Remote dbconsole"
    task :dbconsole, :roles => :app do
      run_interactively "bundle exec rails dbconsole #{rails_env}"
    end


    desc "Run a task on a remote server."
    # run like: cap staging rake:invoke task=a_certain_task
    task :invoke do
      run "cd #{current_path}; /usr/bin/env rake #{ENV['task']} RAILS_ENV=#{rails_env}"
    end

  end

  def run_interactively(command, server=nil)
    hostname = find_servers_for_task(current_task).first
    port = exists?(:port) ? fetch(:port) : 22
    exec "ssh -l #{user} #{hostname} -p #{port} -t 'source ~/.profile && cd #{current_path} && #{command}'"
  end

end
