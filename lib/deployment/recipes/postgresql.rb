Capistrano::Configuration.instance.load do

  set_default(:postgresql_host, "localhost")
  set_default(:postgresql_user)          { user }
  set_default(:postgresql_test_user)     { 'jenkins' }
  set_default(:postgresql_password)      { Capistrano::CLI.password_prompt "PostgreSQL Password: " }
  set_default(:postgresql_database)      { "#{application.gsub('-', '_')}_production" }
  set_default(:postgresql_test_database) { "#{application.gsub('-', '_')}_test" }


  namespace :postgresql do
    desc "Install the latest stable release of PostgreSQL."
    task :install, roles: :db, only: {primary: true} do
      run "#{sudo} apt-get -y install postgresql postgresql-contrib libpq-dev"
    end
    after "deploy:install", "postgresql:install"

    desc "Create a database for this application."
    task :create_database, roles: :db, only: {primary: true} do
      run %Q{#{sudo} -u postgres psql -c "create user #{postgresql_user} with password '#{postgresql_password}';"}
      run %Q{#{sudo} -u postgres psql -c "create database #{postgresql_database} owner #{postgresql_user};"}
    end

    desc "Create a test database for this application."
    task :create_test_database, roles: :db, only: {primary: true} do
      run %Q{#{sudo} -u postgres psql -c "create user jenkins with password '#{postgresql_test_user}';"}
      run %Q{#{sudo} -u postgres psql -c "create database #{postgresql_test_database} owner #{postgresql_test_user};"}
    end

    after "deploy:setup", "postgresql:create_database"
    after "deploy:setup_development", "postgresql:create_test_database"

    desc "Generate the database.yml configuration file."
    task :setup, roles: :app do
      run "mkdir -p #{shared_path}/config"
      template "postgresql.yml.erb", "#{shared_path}/config/database.yml"
    end
    after "deploy:setup", "postgresql:setup"

  end

end
